#!/bin/sh
set -o errexit
set -o nounset

state_file='terraform.tfstate'

if [ -e "$state_file" ]
then
    mv -f "$state_file" "$state_file.backup" \
        || true
else
    rm -f "$state_file"
fi
