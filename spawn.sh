#!/bin/sh
set -o errexit
set -o nounset

$(dirname "$0")/drop.sh
$(dirname "$0")/apply.sh -auto-approve
