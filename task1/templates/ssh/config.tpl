UserKnownHostsFile /dev/null
StrictHostKeyChecking no

Host *
    User ${default_user}
%{~ if ssh_private_key_path != "" }
    IdentityFile ${ssh_private_key_path}
%{~ endif }

Host ${jumphost.alias}
    HostName ${jumphost.hostname}

Host * !${jumphost.alias}
    ProxyJump ${jumphost.alias}
%{~ for host in hosts }

Host ${host.alias}
    HostName ${host.hostname}
%{~ endfor }
