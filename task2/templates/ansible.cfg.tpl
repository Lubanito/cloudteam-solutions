[defaults]
inventory = ${ansible_inventory}
roles_path = ../roles/

[ssh_connection]
pipelining = True
ssh_args = -F ${ssh_config}
